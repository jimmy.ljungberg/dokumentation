# Windows Terminal

## Typsnitt
*Om man installerar [PowerShell](https://gitlab.com/jimmy.ljungberg/dokumentation/-/blob/main/windows/powershell.md) (rekommenderas) med Oh-My-Posh finns det enklare instruktioner där för att installera typsnitt.*

1. Ladda ned font från https://www.nerdfonts.com/font-downloads, förslagsvis `FiraCode Nerd Font`. Som jag förstår det är fonterna patchade och innehåller ikoner.
2. Packa upp vald font och markera alla ttf-filer.
3. Högerklicka på någon av de markerade filerna och välj "Install".
4. Välj installerad font i Windows Terminal.

## Färgschema

### Solarized Dark

Det följer med ett färgschema för Solarized Dark men det stämmer inte med det som anges [här](https://github.com/altercation/solarized#the-values). *black* och *bright black* borde byta plats.

```json
{
    "background": "#002B36",
    "black": "#073642",
    "blue": "#268BD2",
    "brightBlack": "#002B36",
    "brightBlue": "#839496",
    "brightCyan": "#93A1A1",
    "brightGreen": "#586E75",
    "brightPurple": "#6C71C4",
    "brightRed": "#CB4B16",
    "brightWhite": "#FDF6E3",
    "brightYellow": "#657B83",
    "cursorColor": "#657B83",
    "cyan": "#2AA198",
    "foreground": "#839496",
    "green": "#859900",
    "name": "Solarized Dark (corrected)",
    "purple": "#D33682",
    "red": "#DC322F",
    "selectionBackground": "#839496",
    "white": "#EEE8D5",
    "yellow": "#B58900"
}
```

"background", "foreground", "cursorColor" och "selectionBackground" är inte definierade i Solarized färgschema men ingår i färgschema för Windows Terminal. Vill man använda en ännu mörkare bakgrund kan "#001e2a" vara ett lämpligt värde.
