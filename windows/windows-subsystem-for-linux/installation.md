# Installera Windows Subsystem for Linux

1. Starta PowerShell som Administratör och installera WSL:  
   ```powershell
   wsl --list --online
   wsl --install <name>
   ```  
   *(Detta brukar installera Ubuntu som standard också)*
1. Starta om datorn.
1. Uppdatera Ubuntu  
   ```shell
   sudo apt update
   sudo apt full-upgrade
   ```
1. Aktivera systemd  
   Lägg till
   ```
   [boot]
   systemd=true
   ```
   till `/etc/wsl.conf`
1. Starta om Ubuntu  
   ```powershell
   wsl --shutdown
   ```
1. Starta upp Ubuntu  
   ```powershell
   wsl
   ```
