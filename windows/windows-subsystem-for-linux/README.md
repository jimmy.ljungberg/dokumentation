# Windows Subsystem for Linux

* [Installation](installation.md)
* [VPN med Cisco AnyConnect](cisco-anyconnect.md)

## Docker Desktop

Installera inte Docker Desktop om du inte nödvändigtvis vill använda det under Windows. Installera Docker enligt [Dockers egna instruktioner](https://docs.docker.com/engine/install/ubuntu/). Detta förutsätter att systemd aktiverats för WSL.

## Ändra hostname
1. `sudo -i`
1. ```
   cat <<-'EOF' > /etc/wsl.conf
   [network]
   hostname = <hostname>
   generateHosts = false
   EOF
   ```
1. Uppdatera `/etc/hosts` med  ändrat <hostname>

## Explorer

Använd `\\wsl$`

# WSLg

## `Error: Can't open display :0`

Referens:
* https://bytemeta.vip/repo/microsoft/wslg/issues/815
* https://github.com/microsoft/wslg/issues/818#issuecomment-1227756256
* https://github.com/microsoft/wslg/issues/558
* https://docs.nvidia.com/cuda/wsl-user-guide/index.html

```
sudo rm -fr /tmp/.X11-unix
ln -s /mnt/wslg/.X11-unix /tmp/X11-unix
```

## Referens
* https://ubuntu.com/tutorials/install-ubuntu-on-wsl2-on-windows-11-with-gui-support#1-overview
* https://learn.microsoft.com/en-us/windows/wsl/install-manual

## Problem
* https://github.com/arkane-systems/genie/wiki/Systemd-units-known-to-be-problematic-under-WSL

### Error code: Wsl/Service/CreateInstance/MountVhd/ERROR_FILE_NOT_FOUND

Detta kan hända efter man avinstallerat en Linux-distribution.

Mest troligt finns det kvar en registrering för någon distribution.

```powershell
PS C:\Users\jimmy> wsl --list
Windows-undersystem för Linux-distributioner:
Ubuntu (standard)
```

```powershell
wsl --unregister Ubuntu
```

## Backup/restore

Referens: https://www.reddit.com/r/bashonubuntuonwindows/comments/13te686/what_is_the_recommended_way_of_creating_wsl2/

Backup:
```bash
wsl --export <distro> <sökväg>.vhdx --vhd
```

Import:
```bash
wsl --import-in-place <distro> <sökväg>
```

Notera att <distro> kan vara ett godtyckligt namn och p åså sätt kan man ha flera instaner med olika namn.