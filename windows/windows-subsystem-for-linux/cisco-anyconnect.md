# Cisco AnyConnect

## Routing

När Cisco AnyConnect aktivieras så skapar det problem med routing av nätverkstrafik i WSL. Dessutom kan IntelliJ IDEA inte köra tester när koden är lagrad via WSL. Det ställer till det en hel del när jag jobbar hemifrån.

Längre ned i denna fil finns en lösning för att fixa routing till internet i WSL när AnyConnect är aktivierad. Den lösningen hjälper dock inte IntelliJ IDEA.

### AnyConnect inaktiverad

```text
Name                      InterfaceDescription                    ifIndex Status       MacAddress             LinkSpeed
----                      --------------------                    ------- ------       ----------             ---------
VMware Network Adapte...8 VMware Virtual Ethernet Adapter for ...      21 Up           ??-??-??-??-??-??       100 Mbps
VMware Network Adapte...1 VMware Virtual Ethernet Adapter for ...      20 Up           ??-??-??-??-??-??       100 Mbps
Wi-Fi                     Intel(R) Wi-Fi 6E AX211 160MHz               19 Up           ??-??-??-??-??-??       721 Mbps
Ethernet                  Cisco AnyConnect Secure Mobility Cli...       5 Disabled     ??-??-??-??-??-??     717.4 Mbps
Bluetooth Network Conn... Bluetooth Device (Personal Area Netw...       4 Disconnected ??-??-??-??-??-??         3 Mbps
```

```text
===========================================================================
Interface List
 14...?? ?? ?? ?? ?? ?? ......Microsoft Wi-Fi Direct Virtual Adapter
 11...?? ?? ?? ?? ?? ?? ......Microsoft Wi-Fi Direct Virtual Adapter #2
 20...?? ?? ?? ?? ?? ?? ......VMware Virtual Ethernet Adapter for VMnet1
 21...?? ?? ?? ?? ?? ?? ......VMware Virtual Ethernet Adapter for VMnet8
 19...?? ?? ?? ?? ?? ?? ......Intel(R) Wi-Fi 6E AX211 160MHz
  4...?? ?? ?? ?? ?? ?? ......Bluetooth Device (Personal Area Network)
  1...........................Software Loopback Interface 1
 29...?? ?? ?? ?? ?? ?? ......Hyper-V Virtual Ethernet Adapter
===========================================================================

IPv4 Route Table
===========================================================================
Active Routes:
Network Destination        Netmask          Gateway       Interface  Metric
          0.0.0.0          0.0.0.0      192.168.1.1    192.168.1.141     35
        127.0.0.0        255.0.0.0         On-link         127.0.0.1    331
        127.0.0.1  255.255.255.255         On-link         127.0.0.1    331
  127.255.255.255  255.255.255.255         On-link         127.0.0.1    331
      172.19.48.0    255.255.240.0         On-link       172.19.48.1   5256
      172.19.48.1  255.255.255.255         On-link       172.19.48.1   5256
    172.19.63.255  255.255.255.255         On-link       172.19.48.1   5256
     172.31.141.0    255.255.255.0      192.168.1.1    192.168.1.141     36
      192.168.1.0    255.255.255.0         On-link     192.168.1.141    291
    192.168.1.141  255.255.255.255         On-link     192.168.1.141    291
    192.168.1.255  255.255.255.255         On-link     192.168.1.141    291
    192.168.163.0    255.255.255.0         On-link     192.168.163.1    291
    192.168.163.1  255.255.255.255         On-link     192.168.163.1    291
  192.168.163.255  255.255.255.255         On-link     192.168.163.1    291
    192.168.192.0    255.255.255.0         On-link     192.168.192.1    291
    192.168.192.1  255.255.255.255         On-link     192.168.192.1    291
  192.168.192.255  255.255.255.255         On-link     192.168.192.1    291
        224.0.0.0        240.0.0.0         On-link         127.0.0.1    331
        224.0.0.0        240.0.0.0         On-link     192.168.163.1    291
        224.0.0.0        240.0.0.0         On-link     192.168.192.1    291
        224.0.0.0        240.0.0.0         On-link     192.168.1.141    291
        224.0.0.0        240.0.0.0         On-link       172.19.48.1   5256
  255.255.255.255  255.255.255.255         On-link         127.0.0.1    331
  255.255.255.255  255.255.255.255         On-link     192.168.163.1    291
  255.255.255.255  255.255.255.255         On-link     192.168.192.1    291
  255.255.255.255  255.255.255.255         On-link     192.168.1.141    291
  255.255.255.255  255.255.255.255         On-link       172.19.48.1   5256
===========================================================================
Persistent Routes:
  None
```

### AnyConnect aktiverad
```text
Name                      InterfaceDescription                    ifIndex Status       MacAddress             LinkSpeed
----                      --------------------                    ------- ------       ----------             ---------
VMware Network Adapte...8 VMware Virtual Ethernet Adapter for ...      21 Up           ??-??-??-??-??-??       100 Mbps
VMware Network Adapte...1 VMware Virtual Ethernet Adapter for ...      20 Up           ??-??-??-??-??-??       100 Mbps
Wi-Fi                     Intel(R) Wi-Fi 6E AX211 160MHz               19 Up           ??-??-??-??-??-??       721 Mbps
Ethernet                  Cisco AnyConnect Secure Mobility Cli...       5 Up           ??-??-??-??-??-??     717.4 Mbps
Bluetooth Network Conn... Bluetooth Device (Personal Area Netw...       4 Disconnected ??-??-??-??-??-??         3 Mbps
```

```text
===========================================================================
Interface List
  5...?? ?? ?? ?? ?? ?? ......Cisco AnyConnect Secure Mobility Client Virtual Miniport Adapter for Windows x64
 14...?? ?? ?? ?? ?? ?? ......Microsoft Wi-Fi Direct Virtual Adapter
 11...?? ?? ?? ?? ?? ?? ......Microsoft Wi-Fi Direct Virtual Adapter #2
 20...?? ?? ?? ?? ?? ?? ......VMware Virtual Ethernet Adapter for VMnet1
 21...?? ?? ?? ?? ?? ?? ......VMware Virtual Ethernet Adapter for VMnet8
 19...?? ?? ?? ?? ?? ?? ......Intel(R) Wi-Fi 6E AX211 160MHz
  4...?? ?? ?? ?? ?? ?? ......Bluetooth Device (Personal Area Network)
  1...........................Software Loopback Interface 1
 29...?? ?? ?? ?? ?? ?? ......Hyper-V Virtual Ethernet Adapter
===========================================================================

IPv4 Route Table
===========================================================================
Active Routes:
Network Destination        Netmask          Gateway       Interface  Metric
          0.0.0.0          0.0.0.0      192.168.1.1    192.168.1.141     35
          0.0.0.0          0.0.0.0    172.31.141.97   172.31.141.100      2
        127.0.0.0        255.0.0.0         On-link         127.0.0.1    331
        127.0.0.1  255.255.255.255         On-link         127.0.0.1    331
  127.255.255.255  255.255.255.255         On-link         127.0.0.1    331
    130.239.2.231  255.255.255.255      192.168.1.1    192.168.1.141     36
      172.19.48.0    255.255.240.0         On-link       172.19.48.1   5256
      172.19.48.0    255.255.240.0    172.31.141.97   172.31.141.100      2
      172.19.48.1  255.255.255.255         On-link       172.19.48.1   5256
    172.19.63.255  255.255.255.255         On-link       172.19.48.1   5256
    172.31.141.96  255.255.255.224         On-link    172.31.141.100    257
   172.31.141.100  255.255.255.255         On-link    172.31.141.100    257
   172.31.141.127  255.255.255.255         On-link    172.31.141.100    257
      192.168.1.0    255.255.255.0         On-link     192.168.1.141    291
      192.168.1.0    255.255.255.0    172.31.141.97   172.31.141.100      2
      192.168.1.1  255.255.255.255         On-link     192.168.1.141     36
    192.168.1.141  255.255.255.255         On-link     192.168.1.141    291
    192.168.1.255  255.255.255.255         On-link     192.168.1.141    291
    192.168.163.0    255.255.255.0         On-link     192.168.163.1    291
    192.168.163.0    255.255.255.0    172.31.141.97   172.31.141.100      2
    192.168.163.1  255.255.255.255         On-link     192.168.163.1    291
  192.168.163.255  255.255.255.255         On-link     192.168.163.1    291
    192.168.192.0    255.255.255.0         On-link     192.168.192.1    291
    192.168.192.0    255.255.255.0    172.31.141.97   172.31.141.100      2
    192.168.192.1  255.255.255.255         On-link     192.168.192.1    291
  192.168.192.255  255.255.255.255         On-link     192.168.192.1    291
        224.0.0.0        240.0.0.0         On-link         127.0.0.1    331
        224.0.0.0        240.0.0.0         On-link     192.168.163.1    291
        224.0.0.0        240.0.0.0         On-link     192.168.192.1    291
        224.0.0.0        240.0.0.0         On-link     192.168.1.141    291
        224.0.0.0        240.0.0.0         On-link       172.19.48.1   5256
        224.0.0.0        240.0.0.0         On-link    172.31.141.100    257
  255.255.255.255  255.255.255.255         On-link         127.0.0.1    331
  255.255.255.255  255.255.255.255         On-link     192.168.163.1    291
  255.255.255.255  255.255.255.255         On-link     192.168.192.1    291
  255.255.255.255  255.255.255.255         On-link     192.168.1.141    291
  255.255.255.255  255.255.255.255         On-link       172.19.48.1   5256
  255.255.255.255  255.255.255.255         On-link    172.31.141.100    257
===========================================================================
Persistent Routes:
  None
```

### Skillnader
```bash
$ diff get-netadapter-anyconnect-disabled.txt get-netadapter-anyconnect-enabled.txt
7c7
< Ethernet                  Cisco AnyConnect Secure Mobility Cli...       5 Disabled     ??-??-??-??-??-??     717.4 Mbps
---
> Ethernet                  Cisco AnyConnect Secure Mobility Cli...       5 Up           ??-??-??-??-??-??     717.4 Mbps
```

```bash
$ diff route-print-4-anyconnect-disabled.txt route-print-4-anyconnect-enabled.txt
2a3
>   5...?? ?? ?? ?? ?? ?? ......Cisco AnyConnect Secure Mobility Client Virtual Miniport Adapter for Windows x64
17a19
>           0.0.0.0          0.0.0.0    172.31.141.97   172.31.141.100      2
20a23
>     130.239.2.231  255.255.255.255      192.168.1.1    192.168.1.141     36
21a25
>       172.19.48.0    255.255.240.0    172.31.141.97   172.31.141.100      2
24c28,30
<      172.31.141.0    255.255.255.0      192.168.1.1    192.168.1.141     36
---
>     172.31.141.96  255.255.255.224         On-link    172.31.141.100    257
>    172.31.141.100  255.255.255.255         On-link    172.31.141.100    257
>    172.31.141.127  255.255.255.255         On-link    172.31.141.100    257
25a32,33
>       192.168.1.0    255.255.255.0    172.31.141.97   172.31.141.100      2
>       192.168.1.1  255.255.255.255         On-link     192.168.1.141     36
28a37
>     192.168.163.0    255.255.255.0    172.31.141.97   172.31.141.100      2
31a41
>     192.168.192.0    255.255.255.0    172.31.141.97   172.31.141.100      2
38a49
>         224.0.0.0        240.0.0.0         On-link    172.31.141.100    257
43a55
>   255.255.255.255  255.255.255.255         On-link    172.31.141.100    257
```

Detta är vad ChatGPT (Model: GPT-4) hade att säga om utskriften:

>>>
Det verkar som att du har jämfört två olika utskrifter av routingtabellen från en Windows-dator, en med Cisco AnyConnect VPN-anslutningen inaktiverad och en annan med VPN-anslutningen aktiverad. Den diff-kommandoutskrift som du har delat visar skillnaderna mellan dessa två filer.

De viktigaste skillnaderna mellan de två utskrifterna är följande:

1. När AnyConnect är aktiverat, läggs en ny nätverksadapter till, som kallas "Cisco AnyConnect Secure Mobility Client Virtual Miniport Adapter for Windows x64" (rad 2a3).
1. När VPN-anslutningen är aktiverad, läggs det till nya rutter för att dirigera trafiken genom VPN-tunneln. Dessa nya rutter inkluderar:
   * En standardgateway på 172.31.141.97 (rad 17a19).
   * En specifik väg till VPN-servern (130.239.2.231) via den lokala routern (192.168.1.1) (rad 20a23).
   * Vägar till olika nätverk (172.19.48.0, 192.168.1.0, 192.168.163.0 och 192.168.192.0) via VPN-tunneln (rad 21a25, 25a32, 28a37 och 31a41).
1. Några rutter har ändrats för att anpassa sig till VPN-anslutningen, till exempel vägen till det lokala nätverket 172.31.141.0 (rad 24c28,30). När VPN-anslutningen är aktiverad ändras den ursprungliga vägen till ett mer specifikt nätverk (172.31.141.96) och två nya rutter läggs till för att hantera trafiken inom det lokala nätverket och VPN-tunneln.
1. Multicast- och broadcast-adresser läggs till för VPN-adaptern (rad 38a49 och 43a55).

Sammantaget visar dessa skillnader hur routingtabellen uppdateras för att dirigera trafik genom VPN-tunneln när Cisco AnyConnect VPN-anslutningen är aktiverad. Det innebär att viss trafik kommer att skickas genom VPN-tunneln och nå destinationer som annars kanske inte är åtkomliga utan VPN-anslutningen.
>>>

[WSL felrapport](https://github.com/microsoft/WSL/issues/4277)

Nätverksinterface för WSL brukar automatiskt tilldelas värdet 5000 för dess InterfaceMetrics. Det går enkelt att bekräfta via PowerShell:

```powershell
Get-NetIPInterface | Where-Object {$_.InterfaceAlias -Match "WSL"}
```

Det gör att dess interface rankas väldigt lågt vid routing av nätverkstrafik.

Varje gång man startar Cisco AnyConnect så tilldelas dess IPv4 interface värdet 1 för InterfaceMetric (IPv6 verkar tilldelas 6000). Det gör att detta interface får högsta prioritet vid routing av nätverkstrafik, vilket nog är vad man vill vanligtvis.

```powershell
Get-NetAdapter | Where-Object {$_.InterfaceDescription -Match "Cisco AnyConnect"} | Get-NetIPInterface
```

Genom att justera InterfaceMetric till ett värde högre än 5000 kommer routing för WSL att fungera igen. I varje fall för IP-adresser. För att justera InterfaceMetric använd cmdlet `Set-NetIPInterface`. Denna cmdlet kräver dock Administratörsrättigheter.

```powershell
Get-NetAdapter | Where-Object {$_.InterfaceDescription -Match "Cisco AnyConnect"} | Get-NetIPInterface -AddressFamily IPv4 | Set-NetIPInterface -InterfaceMetric 6000
```

Nu fungerar nätverksrouting i WSL. Testa med `ping 8.8.8.8`.

Eftersom dessa inställningar inte sparas permanent har jag underlättat aktiveringen genom att lägga till ett par funktioner i PowerShell.

### Förenkla hantering av VPN

1. Konfigurera [PowerShell](https://gitlab.com/jimmy.ljungberg/dokumentation/-/blob/main/windows/powershell.md)

```powershell
notepad $profile
```

Lägg till följande funktioner:

```powershell
function Get-VPNAdapter {
   Get-NetAdapter | Where-Object {$_.InterfaceDescription -Match "Cisco AnyConnect"}
}

function Get-VPNAdapter-InterfaceMetric {
	Get-VPNAdapter | Get-NetIPInterface -AddressFamily IPv4 | Select-Object InterfaceMetric
}

function Set-VPNAdapter-InterfaceMetric {
	Get-VPNAdapter | Set-NetIPInterface -InterfaceMetric 6000
}
```

Nu räcker det att skriva

```powershell
Set-VPNAdapter-InterfaceMetric
```

Vad som kvarstår är att justera DNS-konfigurationen.

För att visa vilka DNS-servrar AnyConnect använder:

```powershell
(Get-VPNAdapter | Get-DnsClientServerAddress).ServerAddresses
```

I Ubuntu, uppdatera */etc/resolv.conf* med namnservrar som AnyConnect använder.

2025-02-24: Eventuellt behöver man inte längre ändra i */etc/resolv.conf*. Prova först ifall DNS fungerar utan uppdateringar. Det kan också vara så att det fungerade bra för mig på grund av SSH-konfiguration.
## Problem

När jag aktivierar Cisco Anyconnect så går det inte längre att köra tester via IntelliJ IDEA. Helt otroligt! Jag antar det är mer konfiguration som behövs men jag har ingen aning vad som är nästa steg.

### Givet Cisco AnyConnect är aktiv
* Går bra att bygga och exekvera tester via shell och Maven. Detta verkar rimligt eftersom att exekveras inuti Ubuntu.

#### Går inte bra att exekvera tester via IDEA

Vid uppstart av *uttag-server* ser jag följande meddelande:

```
java.util.concurrent.ExecutionException: java.rmi.ConnectIOException: Exception creating connection to: 172.19.50.139; nested exception is: 
	java.net.SocketException: Permission denied: connect
```

```bash
$ ip addr show eth0
6: eth0: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1390 qdisc mq state UP group default qlen 1000
    link/ether 00:15:5d:77:09:bb brd ff:ff:ff:ff:ff:ff
    inet 172.19.50.139/20 brd 172.19.63.255 scope global eth0
       valid_lft forever preferred_lft forever
    inet6 fe80::215:5dff:fe77:9bb/64 scope link
       valid_lft forever preferred_lft forever
```

Så IDEA försöker kommunicera med min Ubuntu-instans men Cisco AnyConnect har på något vis justerat nätverkskonfigurationen så trafiken når inte fram. Är det ännu mer routing-problem?

* Jag provade inaktivera brandväggar i Windows Defender för *Domännätverk*, *Privat nätverk* och *Offentligt nätverk* och startade sedan upp IDEA med projektet *uttag-server*. Jag såg samma meddelande. Detta tycker jag antyder att det inte är Windows Defenders brandväggar som blockerar trafik, vilket känns rimligt med tanke på att jag följt [instruktioner för IntelliJ IDEA](https://www.jetbrains.com/help/idea/how-to-use-wsl-development-environment-in-product.html#debugging_system_settings).

### Referenser
* https://learn.microsoft.com/en-us/windows/wsl/troubleshooting#wsl-has-no-network-connectivity-once-connected-to-a-vpn
* https://gist.github.com/machuu/7663aa653828d81efbc2aaad6e3b1431
* https://gist.github.com/balmeida-nokia/122adf625c11c916902950e3255bd104
* https://www.cisco.com/c/en/us/td/docs/security/vpn_client/anyconnect/anyconnect410/administration/guide/b-anyconnect-admin-guide-4-10/troubleshoot-anyconnect.html#Cisco_Task_in_List_GUI.dita_3a9a8101-f034-4e9b-b24a-486ee47b5e9f
