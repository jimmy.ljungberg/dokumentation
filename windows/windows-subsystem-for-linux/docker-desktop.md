# Docker Desktop

Jag har upptäckt att LXD och Docker Desktops WSL-integration verkar ha problem.

När WSL-integreringen är inaktiverad ("Enable integration with my default WSL distro" är urbockad)

```bash
jilj0001@DATOR:~$ docker ps

The command 'docker' could not be found in this WSL 2 distro.
We recommend to activate the WSL integration in Docker Desktop settings.

For details about using Docker Desktop with WSL 2, visit:

https://docs.docker.com/go/wsl2/

jilj0001@DATOR:~$ lxc list
+------+---------+------+------+-----------+-----------+
| NAME |  STATE  | IPV4 | IPV6 |   TYPE    | SNAPSHOTS |
+------+---------+------+------+-----------+-----------+
| spik | STOPPED |      |      | CONTAINER | 0         |
+------+---------+------+------+-----------+-----------+
```

När WSL-integrering är aktiverad ("Enable integration with my default WSL distro" är ibockad)

```bash
jilj0001@DATOR:~$ docker ps
CONTAINER ID   IMAGE     COMMAND   CREATED   STATUS    PORTS     NAMES
jilj0001@DATOR:~$ lxc list
2023/03/08 20:51:58.718728 system_key.go:129: cannot determine nfs usage in generateSystemKey: cannot parse mountinfo: incorrect number of tail fields, expected 3 but found 4
2023/03/08 20:51:58.723136 cmd_run.go:1046: WARNING: cannot create user data directory: cannot determine SELinux status: failed to obtain SELinux mount path: incorrect number of tail fields, expected 3 but found 4
+------+---------+---------------------+------+-----------+-----------+
| NAME |  STATE  |        IPV4         | IPV6 |   TYPE    | SNAPSHOTS |
+------+---------+---------------------+------+-----------+-----------+
| spik | RUNNING | 10.59.25.150 (eth0) |      | CONTAINER | 0         |
+------+---------+---------------------+------+-----------+-----------+
jilj0001@DATOR:~$ sudo systemctl --failed
  UNIT           LOAD   ACTIVE SUB    DESCRIPTION
● user@0.service loaded failed failed User Manager for UID 0

LOAD   = Reflects whether the unit definition was properly loaded.
ACTIVE = The high-level unit activation state, i.e. generalization of SUB.
SUB    = The low-level unit activation state, values depend on unit type.
1 loaded units listed.
jilj0001@DATOR:~$ sudo systemctl --no-pager status user@0.service
× user@0.service - User Manager for UID 0
     Loaded: loaded (/lib/systemd/system/user@.service; static)
    Drop-In: /usr/lib/systemd/system/user@.service.d
             └─timeout.conf
     Active: failed (Result: exit-code) since Wed 2023-03-08 20:48:39 CET; 3min 53s ago
       Docs: man:user@.service(5)
    Process: 1329 ExecStart=/lib/systemd/systemd --user (code=exited, status=1/FAILURE)
   Main PID: 1329 (code=exited, status=1/FAILURE)

Mar 08 20:48:39 DATOR systemd[1]: Starting User Manager for UID 0...
Mar 08 20:48:39 DATOR systemd[1329]: pam_unix(systemd-user:session): session opened for user root(uid=0) by (uid=0)
Mar 08 20:48:39 DATOR systemd[1329]: pam_systemd(systemd-user:session): Runtime directory '/run/user/0' is not owned by UID 0, as it should.
Mar 08 20:48:39 DATOR systemd[1329]: pam_systemd(systemd-user:session): Not setting $XDG_RUNTIME_DIR, as the directory is not in order.
Mar 08 20:48:39 DATOR systemd[1329]: Trying to run as user instance, but $XDG_RUNTIME_DIR is not set.
Mar 08 20:48:39 DATOR systemd[1]: user@0.service: Main process exited, code=exited, status=1/FAILURE
Mar 08 20:48:39 DATOR systemd[1]: user@0.service: Failed with result 'exit-code'.
Mar 08 20:48:39 DATOR systemd[1]: Failed to start User Manager for UID 0.
```
