# VPN

Cisco AnyConnect verkar ändra så mycket.

Ett problem med AnyConnect är att jag tappar internet i WSL.

Lösning: https://gist.github.com/balmeida-nokia/122adf625c11c916902950e3255bd104

Min egen dokumentation: https://gitlab.com/kilathaar/dokumentation/-/blob/main/windows/windows-subsystem-for-linux/cisco-anyconnect.md

 <del>Jag föredrar att i stället använda [OpenConnect](https://github.com/openconnect/openconnect-gui/releases) i stället.</del>
 *(Lösningen gav mig 52 gånger sämre överföringshastighet än AnyConnect)*
