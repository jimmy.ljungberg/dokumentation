# Scoop

Exekvera i PowerShell:

```powershell
Set-ExecutionPolicy RemoteSigned -Scope CurrentUser
```

```powershell
iwr -useb get.scoop.sh | iex
```

Det verkar finnas många intressanta program som går att installera. Frågan är dock hur intressanta de är att använda om man ändå tänker använda WSL.

```bash
scoop install fzf jq lsd pandoc
```
* Pip: https://www.geeksforgeeks.org/how-to-install-pip-on-windows/

## Scoop och SSH
* https://scoop-docs.vercel.app/docs/guides/SSH-on-Windows.html#create-a-key-for-authentication
