# SSH

* https://medium.com/@Devin007/install-ssh-agent-and-ssh-add-to-powershell-c047b36864e3
* https://learn.microsoft.com/en-us/windows-server/administration/openssh/openssh_keymanagement?source=recommendations
* https://learn.microsoft.com/en-us/windows-server/administration/openssh/openssh_install_firstuse?tabs=gui
* https://gist.github.com/danieldogeanu/16c61e9b80345c5837b9e5045a701c99

Med SSH brukar det vara praxis att använda sig av en `ssh-agent` för att förenkla användandet av SSH. Den lagrar vanligtvis nycklar i minnet och låter dig slippa ange lösenordet nästa gång nyckeln används.

## Windows

I Windows 11 *(jag vet inte hur det är med Windows 10 eller tidigare)* ingår en portning av OpenSSH inklusive `ssh-agent`. Men denna version lagrar din nyckel i registret och du behöver inte ange lösenordet efter en omstart av datorn. Det är mer bekvämt men också en större risk eftersom man aldrig mer behöver bekräfta sig för att använda nyckeln.

Som standard är `ssh-agent` konfigurerad att startas manuellt.

Jag hittade denna [rapport](https://github.com/PowerShell/Win32-OpenSSH/issues/1487) och kunde bekräfta att nycklar sparas under `HKEY_CURRENT_USER\Software\OpenSSH\Agent\Keys`.

[Denna kommentar](https://github.com/PowerShell/Win32-OpenSSH/issues/1487#issuecomment-983055919) indikerar att det kommer gå att använda `ssh-agent` på ett sätt som inte lagrar nyckeln i registret.

2022-07-02: Så fungerar det inte i dagsläget utan nycklar lagras i registrert även om man startar upp `ssh-agent` interaktivt. Som det nu är tycker jag denna implementering av `ssh-agent` är skadlig och bör inaktivieras.

Bekräfta status för Windows `ssh-agent`:
```powershell
Get-Service ssh-agent
```

För att aktivera automatisk start av ssh-agent skriv följande i ett upphöjt PowerShell:
```powershell
Set-Service ssh-agent -StartupType Automatic
```

```powershell
Start-Service ssh-agent
```

[wsl-ssh-agent](https://github.com/rupor-github/wsl-ssh-agent) verkar intressant

## Git Bash
https://docs.github.com/en/authentication/connecting-to-github-with-ssh/working-with-ssh-key-passphrases
