# PowerShell

Windows 11 levereras med PowerShell 5.*, (Sökvägen är `%SystemRoot%\System32\WindowsPowerShell\v1.0\powershell.exe`) men det finns nyare versioner och det blir enkelt att installera via [winget](https://learn.microsoft.com/en-us/windows/package-manager/winget/) (ingår i Windows 11).

Vad som är viktigt att känna till är att *konfiguration* för Microsoft PowerShell och PowerShell inte använder samma inställningar. **Därför föreslår jag att man först bara installerar den nyaste PowerShell via winget och sedan uppdaterar sökvägen för PowerShell i konfigurationen av Windows Terminal.**

 För att ta reda på vilka versioner som finns kan du i PowerShell skriva följande:

```powershell
winget search Microsoft.PowerShell
```

```PowerShell
winget install --id Microsoft.Powershell --source winget
```

Den nya versionen av PowerShell har nu installerats under `C:\Program Files\PowerShell\7` och exe-filen har namnet `pwsh.exe`.

## konfiguration

1. Justera [PowerShell Execution policy](https://docs.microsoft.com/sv-se/powershell/module/microsoft.powershell.core/about/about_execution_policies?view=powershell-7.2)  (kräver administrativa rättigheter)
   ```powershell
   Set-ExecutionPolicy RemoteSigned
   ```
1. Skapa en konfigurationsfil för PowerShell  
   ```powershell
   new-item $profile -itemtyp file -force
   ```
1. ```
   notepad $profile
   ```
1. Lägg till *(Jag vill minnas att det aktiverar Tab-completion)*  
   ```powershell
   Set-PSReadlineKeyHandler -Key Tab -Function MenuComplete
   ```
1. Installera Oh-My-Posh:  
   ```powershell
   winget install JanDeDobbeleer.OhMyPosh --source winget
   ```
1. Öppna nytt PowerShell
1. Uppdatera $profile:  
   ```powershell
   oh-my-posh init pwsh | Invoke-Expression
   ```

1. Starta powershell med administrativa rättigheter och installera ett typsnitt (t ex SauceCodePro Nerd Font Mono (SourceCode Pro patchad med glyphs och ikoner)):  
   ```
   oh-my-posh font install
   ```

1. Uppdatera inställningar för Windows Terminal att använda det nya typsnittet.

### ssh-agent
Jag har inte någon riktigt bekväm lösning än, men via `notepad $profiel` lägg till:
```powershell
ssh-add $ENV:UserProfile\.ssh\id_ed25519
```  
Ovanstående fungerar, men det tar ingen hänsyn om det redan är utfört sedan tidigare. Man kommer alltid att behöva ange lösenordet, även om nyckeln redan är laddad. FÖr stunden är det bättre att bara manuellt exekvera kommandot `ssh-add` första gången man loggar in.

Eftersom det inte sker någon kontroll ifall nyckel redan laddats kommer varje uppstart av PowerShell att vilja exekvera `ssh-add ...`.

## Enhetstester
Ramverket [Pester](https://github.com/pester/Pester)

## Referenser
* https://ohmyposh.dev/
* https://learn.microsoft.com/en-us/powershell/scripting/learn/shell/creating-profiles?view=powershell-7.3
* https://www.makeuseof.com/windows-powershell-commands-cmdlets/
* https://github.com/Pscx/Pscx
* https://lazyadmin.nl/powershell/powershell-commands/
* https://www.sans.org/blog/getting-started-with-powershell/
