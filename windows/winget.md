# Winget

Repository: https://winget.run/

* [cURL](https://winget.run/pkg/cURL/cURL):  
  ```powershell
  winget install -e --id cURL.cURL
  ```
* [Docker desktop](https://winget.run/pkg/Docker/DockerDesktop):  
   ```powershell
   winget install -e --id Docker.DockerDesktop
   ```
* [eza](https://github.com/eza-community/eza/tree/main):  
   ```powershell
   winget install eza-community.eza
   ```
* [git](https://winget.run/pkg/Git/Git):  
  ```powershell
  winget install -e --id Git.Git
  ```
  Konfigurera git att använda SSH som ingår i Windows 11:  
  ```powershell
  git config --global core.sshCommand "'C:\Windows\System32\OpenSSH\ssh.exe'"
  ```
* [nvm](https://winget.run/pkg/CoreyButler/NVMforWindows):  
  ```powershell
  winget install -e --id CoreyButler.NVMforWindows
  ```
* [Microsoft Visual Studio Code](https://winget.run/pkg/Microsoft/VisualStudioCode):  
   ```powershell
   winget install -e --id Microsoft.VisualStudioCode
   ```
* [JDK 19](https://winget.run/pkg/EclipseAdoptium/Temurin.19.JDK):  
   ```powershell
   winget install -e --id EclipseAdoptium.Temurin.19.JDK
   ```
* [less](https://github.com/jftuga/less-Windows):  
   ```powershell
   winget install jftuga.less
   ```
* [unzip](https://winget.run/pkg/GnuWin32/UnZip):  
   ```powershell
   winget install -e --id GnuWin32.UnZip
   ```

## Referens
* https://www.computerworld.com/article/3684171/winget-the-best-way-to-keep-windows-apps-updated.html
