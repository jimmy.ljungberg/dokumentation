# QEMU
Referens: https://wiki.archlinux.org/title/QEMU

## Avbild av hårddisk

Avbild är antingen i formatet *raw* (Allokerar allt diskutrymme, bättre prestanda) eller *qcow2* (Allokerar diskutrymme dynamiskt, långsammare).

```bash
qemu-img create -f raw <filnamn> <storlek>
```
```bash
qemu-img create -f qcow2 <filnamn> <storlek>
```
