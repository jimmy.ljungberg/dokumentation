# Virtualisering

Referenser:
* https://wiki.archlinux.org/title/PCI_passthrough_via_OVMF
* https://wiki.archlinux.org/title/Libvirt
* https://aur.archlinux.org/packages/virtio-win
* https://www.youtube.com/playlist?list=PLG7vUqRxMOG6gsPXohhFht3UJbcCxYgcL
* https://sysguides.com/install-a-windows-11-virtual-machine-on-kvm
* https://sysguides.com/install-kvm-on-linux

## Anteckningar

### VirtIO Disk
* Cache mode: writeback
* Discard mode: unmap

*Jag vet inte riktigt hur mycket det gör för prestandan.*

### Annat

*/etc/modprobe.d/amdgpu.conf*
```text
options amdgpu si_support=1
options amdgpu cik_support=1
```

*/etc/modprobe.d/vfio.conf*
```text
options vfio-pci ids=10de:2204,10de:1aef
softdep drm pre: vfio-pci
```

*/boot/loader/entries/arch-system.conf*
```text
title   Arch Linux
linux   /vmlinuz-linux
initrd  /initramfs-linux.img
options root=/dev/mapper/system-root rw iommu=pt
```

## Windows 11

### VM

#### Overview
Hypervisor: KVM
Chipset: Q35
Firmware: UEFI x86_64: /usr/share/edk2/x64/OVMF_CODE.fd

#### CPUs
Topology:
Sockets: 1
Cores: 14
Threads: 2

#### HyperV
```xml
<hyperv mode="custom">
    <relaxed state="on"/>
    <vapic state="on"/>
    <spinlocks state="on" retries="8191"/>
    <vpindex state="on"/>
    <runtime state="on"/>
    <synic state="on"/>
    <stimer state="on">
    <direct state="on"/>
    </stimer>
    <reset state="on"/>
    <vendor_id state="on" value="KVM Hv"/>
    <frequencies state="on"/>
    <reenlightenment state="on"/>
    <tlbflush state="on"/>
    <ipi state="on"/>
</hyperv>
```

#### TPM
Model: `CRB`
Version: `2.0`

#### installation
Ta bort nätverksinterface under installation. Detta gör att Windows 10 kommer erbjuda dig att skapa en lokal användare utan att koppla den till ett Microsoft-konto.

Nu kan jag påbörja Windows-installationen. När installationen kommer till att vilja ansluta till internet, tryck `Shift + F10` för att öppna ett konsollfönster.  Skriv sedan `OOBE\BYPASSNRO`. Installationen kommer starta om och du får möjlighet att skapa en användare utan att koppla den till ett Microsoft-konto. Stäng ned när installationen är klar och gör följande modifieringen av VM:

* Ta bort CD-ROM
* Skapa en minimal hårddisk (0,1 GB), Device Type: Disk och Bus Type VirtIO. Denna används för att installera VirtIO-drivrutiner.
* Lägg till VirtIO-iso
* Lägg till GPU via PCI HOst Device
* Lägg till nätverksinterface
