# libvirt
Referens: https://wiki.archlinux.org/title/libvirt

En samling verktyg som underlättar hantering av virtualisering via kommandot `virsh`.

## Storage pool
Referens: https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/6/html/virtualization_administration_guide/sect-virtualization-storage_pools-creating-local_directories-virsh

Skapa en pool:
```bash
virsh pool-define-as <pool-namn> dir - - - - "<fullständig sökväg till katalog>"
```

Lista tillgängliga pooler:
```bash
virsh pool-list --all
```
