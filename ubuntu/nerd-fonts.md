# Nerd fonts

Referens: https://www.nerdfonts.com/

Typsnitt som patchats med extra ikoner (powerline symbol, font awesome, Material icons, Font logos, Weather icons, Devicon).

## Installation

1. Ladda hem önskade [typsnitt](https://www.nerdfonts.com/font-downloads).  
   *(Jag brukar använda "Fira Code" eller "JetBrainsMono")*
1. Packa upp zip-filerna i din lokala katalog för typsnitt.
   ```bash
   unzip ~/Downloads/FiraCode.zip -d ~/.fonts
   ```
1. Uppdatera cache för typsnitt
   ```bash
   fc-cache -fv
   ```

Börja använd de nya typsnitten. (Kan behöva starta om terminal)
