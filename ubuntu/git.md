# git

```shell
sudo apt install --yes --no-install-recommends git
```

## Konfiguration
```shell
set username="Your Name"
set useremail="you@example.com"
```

```shell
git config --global init.defaultBranch main && \
git config --global user.name $username && \
git config --global user.email $useremail
```
