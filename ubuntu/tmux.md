# tmux

## Installation

```bash
sudo apt install --no-install-recommends tmux
```

## Skapa initial konfiguration

** OBSERVERA** Detta skriver över eventuell befintligt konfiguration

```shell
cat <<-'EOF' > $HOME/.tmux.conf
set -g base-index 1
set -g mouse on
set -g aggressive-resize on
set-environment -g 'IGNOREEOF' 2

bind-key r source-file ~/.tmux.conf \; display-message "~/.tmux.conf reloaded"

# Kill pane without confirmation
bind-key & kill-window
bind-key x kill-pane
EOF
```

## Bash completion

```shell
mkdir -p ~/.local/share/bash-completion/completions
```

```shell
curl https://raw.githubusercontent.com/imomaliev/tmux-bash-completion/master/completions/tmux --output ~/.local/share/bash-completion/completions/tmux.bash
```
