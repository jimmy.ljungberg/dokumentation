# exa

Exa är en modern ersättning för ls.

**OBS!** Exa underhålls ej längre, se [eza](https://gitlab.com/jimmy.ljungberg/dokumentation/-/blob/main/ubuntu/eza.md)

Referes: https://github.com/ogham/exa

## Installation

```bash
sudo apt install --yes --no-install-recommends exa
```
