# Installation
1. ```shell
   sudo snap install lxd
   ```
1. ```shell
   sudo lxd init
   ```  
   Jag brukar välja default-värden förutom för IPv6 där jag brukar välja `none`
1. ```shell
   sudo usermod -a -G lxd $USER
   ```
Logga ut och in för att rättigheter för LXD skall aktiveras.
