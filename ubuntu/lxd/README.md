# LXD

2024-02-11: Efter Canonicals ändringar har jag bytt till [Incus](https://gitlab.com/jimmy.ljungberg/dokumentation/-/blob/main/ubuntu/incus.md?ref_type=heads)

* [Installation](installation.md)
* [Docker](docker.md)

## "Snabbstart"

```shell
lxc launch ubuntu <namn>
```

```shell
lxc shell <namn>
```

Förutsatt att Docker inte installerats borde containern ha tillgång till internet. FInns Docker installerat behöver man justera brandväggen (Se länk ovan).

## Inaktivera nätverk för en container

Maskera befintligt interface `eth0` genom att ersätta det med:  
`lxc config device add <container name> eth0 none`

Vill man återställa nätverket tar man bara bort maskeringen med  
`lxc config device remove <container name> eth0`

## Referenser

* https://linuxcontainers.org/lxd/getting-started-cli/
* https://discuss.linuxcontainers.org/t/using-static-ips-with-lxd/1291
* https://ubuntu.com/tutorials/how-to-run-docker-inside-lxd-containers#1-overview
* https://vitorafgomes.medium.com/install-docker-on-lxd-8ff60cd588c
* https://linuxcontainers.org/lxd/docs/master/howto/network_bridge_firewalld/
