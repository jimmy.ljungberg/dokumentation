# RAM-disk

## Manuellt

1. Kontrollera om kernel har laddat modul för RAM-disk
   ```bash
   lsmod | grep brd
   ```
   Om inget returneras så är modulen inte laddad in.
1. För att manuellt ladda modulen skriv:
   ```bash
   sudo modprobe brd rd_nr=1 rd_size=4194304
   ```
   Detta preparerar en (1) RAM-disk på 4 GB (4194304 KB) och device `/dev/ram0` borde vara tillgängligt.
1. Skapa ett filsystem för `/dev/ram0`
   ```bash
   sudo mkfs.ext4 /dev/ram0
   ```
1. Skapa monteringspunkt för RAM-disk
   ```bash
   mkdir /mnt/ramdisk
   ```
1. Montera RAM-disk
   ```bash
   sudo mount /dev/ram0 /mnt/ramdisk
   ```

### Automatiskt

1. Lägg till`brd` till `/etc/modules`
1. Skapa `/etc/modprobe.d/brd.conf`
   ```text
   options rd_nr=1 rd_size=4194304
   ```
1. Uppdatera initramfs
   ```bash
   sudo update-initramfs -u
   ```
1. Skapa systemd-service som förbereder RAM-disk (`/etc/systemd/system/ramdisk.service`)
   ```systemd
   [Unit]
   Description=Create and Mount RAM Disk
   After=local-fs.target

   [Service]
   Type=oneshot
   ExecStart=/usr/sbin/mkfs.ext4 /dev/ram0
   ExecStartPost=/bin/mkdir -p /mnt/ramdisk
   ExecStartPost=/bin/mount /dev/ram0 /mnt/ramdisk
   ExecStartPost=/bin/chown your_user:your_user /mnt/ramdisk
   RemainAfterExit=yes

   [Install]
   WantedBy=default.target
   ```
1. Aktivera och starta service:
   ```bash
   sudo systemctl enable ramdisk.service
   sudo systemctl start ramdisk.service
   ```