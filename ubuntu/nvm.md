# NVM

## Installation

*[Kontrollera ifall detta verkligen är senaste versionen](https://github.com/nvm-sh/nvm?tab=readme-ov-file#installing-and-updating)*
```
curl -o- https://raw.githubusercontent.com/nvm-sh/nvm/v0.39.7/install.sh | bash
```

## Konfiguration

```shell
nvm install --lts
```
