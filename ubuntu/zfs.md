# ZFS

På min arbetsdator har jag märkt att när jag installerat Ubuntu och använt ZFS så visas ett flertal felutskrifter vid uppstart som inte visas när jag ej använder ZFS.

```
find: /dev/zvol/: No such file or directory
find: /dev/zvol/: No such file or directory
[FAILED] Failed to start Load Kernel Module efi_pstore
[FAILED] Failed to start Load Kernel Module pstore_blk
[FAILED] Failed to start Load Kernel Module pstore_zone
[FAILED] Failed to start Load Kernel Module ramoops
[FAILED] Failed to start Load Kernel Module efi_pstore
[FAILED] Failed to start Load Kernel Module pstore_blk
[FAILED] Failed to start Load Kernel Module pstore_zone
[FAILED] Failed to start Load Kernel Module ramoops
[FAILED] Failed to start Load Kernel Module efi_pstore
[FAILED] Failed to start Load Kernel Module pstore_blk
[FAILED] Failed to start Load Kernel Module pstore_zone
[FAILED] Failed to start Load Kernel Module ramoops
```

* [Bugg-rapport](https://bugs.launchpad.net/ubuntu/+source/systemd/+bug/1982462)

Referenser:
* https://manpages.ubuntu.com/manpages/focal/en/man5/pstore.conf.5.html
* https://manpages.ubuntu.com/manpages/jammy/man8/systemd-pstore.service.8.html

Vid en installation undvek jag Ubuntus installation och provade istället [Root on ZFS](https://openzfs.github.io/openzfs-docs/Getting%20Started/Ubuntu/Ubuntu%2022.04%20Root%20on%20ZFS.html) för Ubuntu 22.04.1 LTS.

Allt gick bra ändra till näst sista punkten (punkt 10) i steg 5 där kommandot `zpool export -a` klagade på att något var monterat när det kommandot exekverades.

Jag vill även minnas att ZFS föredrog att få tillgång till hela disken istället för enbart en partition men jag vet inte varför eller om det fortfarande är aktuellt. [Följande tråd](https://openzfs.github.io/openzfs-docs/Getting%20Started/Ubuntu/Ubuntu%2022.04%20Root%20on%20ZFS.html) verkar säga att om man endast har en disk spelar det ingen roll.

## Återställ dataset från backup

Det handlar bara om att dirigera data från backup-fil till `zfs receive`
```bash
zfs receive <dataset> < <backupfil>
```
Vad som verkar viktigt att komma ihåg är i fall zvol var skapad med `-s` (sparse) då behöver jag ange `-s` även i `zfs receive`.

## Lägg till en till disk efter installation

Referens: http://free_zed.gitlab.io/articles/2021/01/extend-zfs-root-pool-after-ubuntu-installation/en/

