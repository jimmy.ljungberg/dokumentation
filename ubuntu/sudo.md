## Sudo utan lösenord
Skapa filen `/etc/sudoers.d/<username>`

```shell
<username>    ALL=(ALL)    NOPASSWD:ALL
```

Ersätt `<username>` med användarens identitet. Filen måste sedan tilldelas filrättigheterna `0440`.

### Begräns sudo till specifik användare:
```shell
<username>    ALL=(<username-man-kan-göra-sudo-till>)    NOPASSWD:ALL
```

Referenser:
* https://www.networkworld.com/article/964736/building-command-groups-with-sudo.html
* https://superuser.com/questions/767415/limit-user-to-execute-selective-commands-linux
* https://askubuntu.com/questions/654832/giving-limited-sudo-privilege-to-a-user