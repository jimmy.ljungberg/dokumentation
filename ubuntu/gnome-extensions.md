# GNOME extensions

GNOME extension manager låter dig söka och installera GNOME extensions.

Leta efter extension via https://extensions.gnome.org/ och installera dom sedan från extension manager.

Följande extension brukar jag vilja använda:

* [Workspace Matrix](https://extensions.gnome.org/extension/1485/workspace-matrix/)
* [Frippery Move Clock](https://extensions.gnome.org/extension/2/move-clock/)
* [Tactile](https://extensions.gnome.org/extension/4548/tactile/)
* <del>[Smart Auto Move](https://extensions.gnome.org/extension/4736/smart-auto-move/)</del>
* <del>[Nano System Monitor](https://extensions.gnome.org/extension/5037/nano-system-monitor/)</del>

