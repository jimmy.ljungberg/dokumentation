# GNOME Terminal

## Typsnitt anpassad för [Powerline](powerline.md)

1. `cd ~/Downloads`
1. `git clone https://github.com/powerline/fonts.git --depth=1 fonts`
1. `./fonts/install.sh`
1. Logga ut och in
1. Byt till font "Source Code Pro for Powerline Regular"

## Solarized färgschema
Referens: https://github.com/aruhier/gnome-terminal-colors-solarized

1. `sudo apt install dconf-cli`
1. `git clone https://github.com/aruhier/gnome-terminal-colors-solarized.git`
1. `cd gnome-terminal-colors-solarized`
1. `./install.sh`

Om du även installerar konfiguration för dircolors (rekommenderas) så behöver du antagligen justera `.bashrc`. Vanligtvis brukar det stå
```bash
if [ -x /usr/bin/dircolors ]; then
    test -r ~/.dircolors && eval "$(dircolors -b ~/.dircolors)" || eval "$(dircolors -b)"
```

Det behöver ändras till
```bash
if [ -x /usr/bin/dircolors ]; then
    test -r ~/.dir_colors && eval "$(dircolors -b ~/.dir_colors/dircolors)" || eval "$(dircolors -b)"
```

## Starta om GNOME Shell (fungerar ej för Wayland)
1. `Alt + F2`
1. `r`
1. `Enter`

## Byt till visual bell
1. `gsettings set org.gnome.desktop.wm.preferences audible-bell false`
1. `gsettings set org.gnome.desktop.wm.preferences visual-bell true`

Man kan justera hur kraftig blinkningen skall vara med:
* `gsettings set org.gnome.desktop.wm.preferences visual-bell-type frame-flash`
* `gsettings set org.gnome.desktop.wm.preferences visual-bell-type fullscreen-flash`
