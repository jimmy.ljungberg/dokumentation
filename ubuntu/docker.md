# Docker

Installera enligt [Dockers egna instruktioner](https://docs.docker.com/engine/install/ubuntu/)

Lägg till din användare till grupper för Docker:

```shell
sudo usermod -a -G docker $USER
```

Logga ut och in för att ändringar skall ta effekt.

## Aktivera Docker daemon vid uppstart

```shell
sudo systemctl enable docker.service
sudo systemctl enable containerd.service
```

För att starta Docker manuellt använd

```shell
sudo systemctl start docker
```

## ZFS
Referens:
* https://www.reddit.com/r/docker/comments/smtzho/docker_and_zfs_looks_like_a_mess/
* https://github.com/moby/moby/issues/40132
* https://github.com/moby/moby/issues/41055

2022-09-05: 
Under ZFS använder docker sig av [ZFS Storage Driver](https://docs.docker.com/storage/storagedriver/zfs-driver/) och sparar "lagren" i Docker som egna dataset.

Den lösning som för stunden verkar bekvämast är att "lura" Docker genom att skapa en ZVOL med filsystemet ext4 och montera det under `/var/lib/docker` innan man installerar Docker.

```bash
sudo zfs create -V 20G rpool/docker
sudo mkfs.ext4 /dev/rpool/docker
sudo mkdir -p /var/lib/docker
sudo mount /dev/rpool/docker /var/lib/docker
sudo tail -1 /etc/mtab | sudo tee -a /etc/fstab
```
