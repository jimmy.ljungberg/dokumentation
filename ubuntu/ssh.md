# SSH

```shell
sudo apt install openssh-client keychain
```

## Konfiguration

```shell
cat <<-'EOF' >> $HOME/.bashrc
# For Loading the SSH key
/usr/bin/keychain --nogui $HOME/.ssh/id_ed25519
source $HOME/.keychain/$(hostname)-sh
EOF
```

## Återskapa publik nyckel från privat

Det verkar som att i varje fall [GNOME keyring](https://wiki.gnome.org/Projects/GnomeKeyring/Ssh) bara fungerar när det finns både privat och publik nyckel.

```bash
ssh-keygen -f ~/.ssh/id_ed25519 -y > ~/.ssh/id_ed25519.pub
```

## Tvinga starta bash utan inläsning av konfigurationsfiler

Det har hänt att mina konfigurationsfiler har innehållt så felaktiga instruktioner att jag inte fått access till shell-prompt via ssh. Om sådant händer är detta väldigt användbart:

```bash
ssh -t user@remote-server bash --noprofile --norc
```

Detta startar upp bash utan att läsa in några konfigurationsfiler som helst och ger mig en chans att rätta till problemet.
