# X11

## Avinstallera X11

Referenser:
* https://linuxconfig.org/how-to-disable-enable-gui-on-boot-in-ubuntu-20-04-focal-fossa-linux-desktop
* https://askubuntu.com/questions/73219/remove-packages-to-transform-desktop-to-server

Inaktiviera uppstart av GUI:
```bash
sudo systemctl set-default multi-user
```

Avinstallera allt som har med X11 att göra, inklusive appar:
```bash
sudo apt-get purge libx11.* libqt.*
```

följt av

```bash
sudo apt-get autoremove
```

