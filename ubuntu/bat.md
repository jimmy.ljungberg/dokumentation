# bat

Referens: https://github.com/sharkdp/bat

A cat(1) clone with syntax highlighting and Git integration. 

## Installation

### Ubuntu

```bash
sudo apt install --yes --no-install-recommends bat
```
