# VPN

*Nedanstående fungerar inte längre sedan UmU ändrade hur autentisering fungerar.*

1. `sudo apt install openvpn openconnect network-manager-openconnect network-manager-openconnect-gnome`

Konfigurera sedan en anslutning via "Settings / Network VPN". Använd "Multi-protocol VPN client (openconnect)"

# Cisco AnyConnect

Referens: https://community.cisco.com/t5/vpn/cisco-anyconnect-disconnects-every-5-minutes-then-reconnects/td-p/4400917

Inaktivera "Connectivity Checking" under "Settings / Privacy / Connectivity" för en stabilare uppkoppling.
