# "Felsökning"

* [Wayland](https://discourse.ubuntu.com/t/why-ubuntu-22-04-is-so-fast-and-how-to-make-it-faster/30369/4)
## dmesg

```bash
sudo dmesg --level err
```

Filtrerar ut endast de meddelanden som är fel. Parametern kan även kombineras med flera logg-typer t ex `--level err,warn`.

### [drm:dcn10_register_irq_handlers [amdgpu]] *ERROR* Failed to register vline0 irq 30!
* https://gitlab.freedesktop.org/drm/amd/-/issues/1933

### mtd device must be supplied (device name is empty)
* https://bugs.launchpad.net/ubuntu/+source/systemd/+bug/1981622
* https://askubuntu.com/questions/1417618/mtd-device-must-be-supplied-device-name-is-empty

## journalctl

```bash
sudo journalctl -p err -xb
```

Där

* `-x` betyder visa extra information
* `-b`betyder visa infomration sedan senaste boot

```bash
systemctl list-units --state=failed
  UNIT                         LOAD   ACTIVE SUB    DESCRIPTION                   
● modprobe@efi_pstore.service  loaded failed failed Load Kernel Module efi_pstore
● modprobe@pstore_blk.service  loaded failed failed Load Kernel Module pstore_blk
● modprobe@pstore_zone.service loaded failed failed Load Kernel Module pstore_zone
● modprobe@ramoops.service     loaded failed failed Load Kernel Module ramoops

LOAD   = Reflects whether the unit definition was properly loaded.
ACTIVE = The high-level unit activation state, i.e. generalization of SUB.
SUB    = The low-level unit activation state, values depend on unit type.
4 loaded units listed.
```
Felrapport: https://bugs.launchpad.net/ubuntu/+source/systemd/+bug/1982462

Lösningen på problemet verkar vara att redigera filen `/usr/lib/systemd/system/modprobe@.service` och lägga till raden

```
StartLimitIntervalSec=0
```

Innehållet i filen ser du ut:

```
Before=sysinit.target
Documentation=man:modprobe(8)
ConditionCapability=CAP_SYS_MODULE
StartLimitIntervalSec=0

[Service]
Type=oneshot
```

Jag har provat detta och nu visar `systemctl status` inte längre degraded.

```bash
journalctl -rb -1 | nc termbin.com 9999
```

>>>
That command prints the journal log information of the prior boot cycle (the one which you stopped with the power button) in reverse order (so the reason for the hang should be near the top) and sends it to termbin. It will return with a url address that you should post should you want us to help you analyze it.
>>>
