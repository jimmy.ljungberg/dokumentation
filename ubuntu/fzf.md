# Fzf

Referens:
  * https://github.com/junegunn/fzf?tab=readme-ov-file
  * https://medium.com/pragmatic-programmers/find-anything-you-need-with-fzf-the-linux-fuzzy-finder-tool-f48dfd0092b4

fzf is a general-purpose command-line fuzzy finder.

## Installation

```bash
sudo apt install --yes --no-install-recommends git
```

```bash
git clone --depth 1 https://github.com/junegunn/fzf.git ~/.local/fzf
```

```bash
~/.local/fzf/install
```

*Aktivera "fuzzy auto-completion" och "keybindings")*

## Anpassning

Som standard använder *fzf* *find* för att hitta filer. Genom att installera [fd](https://gitlab.com/jimmy.ljungberg/dokumentation/-/blob/main/ubuntu/fd.md?ref_type=heads) kan vi effektiviera det lite mer.

```bash
export FZF_DEFAULT_COMMAND="fd --type f"
```
