# IPtables

Referens: https://help.ubuntu.com/community/IptablesHowTo

Vanligtvis är ändringar i iptables bara tillfälliga och persisteras inte. För att spara ändringar använd kommandona `iptables-save` och `iptables-restore`.

Enklast är dock att installera *iptables-persistent*. Under installationen blir du tillfrågad om du vill spara undan de aktuella reglera och automatiskt ladda in dom.

```bash
sudo apt-get install iptables-persistent
```
