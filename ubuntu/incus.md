# [Incus](https://linuxcontainers.org/incus/)

* [Installation](https://github.com/zabbly/incus?tab=readme-ov-file#installation)
* [Getting started](https://linuxcontainers.org/incus/docs/main/tutorial/first_steps/) (Använd inte argument `--minimal`, ger IPv6 konfiguration också)

## Docker och Incus

Referens: https://linuxcontainers.org/incus/docs/main/howto/network_bridge_firewalld/#prevent-connectivity-issues-with-incus-and-docker

Lägg till följande regler till iptables:

```shell
iptables -I DOCKER-USER -i <network_bridge> -j ACCEPT
iptables -I DOCKER-USER -o <network_bridge> -m conntrack --ctstate RELATED,ESTABLISHED -j ACCEPT
```

Där `<network_bridge>` vanligtvis är `incusbr0` (standardnamn som anges under installation)

```shell
iptables -I DOCKER-USER -i incusbr0 -j ACCEPT
iptables -I DOCKER-USER -o incusbr0 -m conntrack --ctstate RELATED,ESTABLISHED -j ACCEPT
```

Se [iptables](https://gitlab.com/jimmy.ljungberg/dokumentation/-/blob/main/ubuntu/iptables.md?ref_type=heads) för instruktioner om att persistera regler för brandväggen.
