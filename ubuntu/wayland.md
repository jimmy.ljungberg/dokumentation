# Wayland

*Jag vet inte längre hur aktuellt detta är*

Referens: https://www.secjuice.com/wayland-vs-xorg/

För att aktiviera `unsafe_mode` i Wayland *(så att du t ex kan dela skärm i Zoom)*:
1. `Alt+F2`
1. `lg`
1. `global.context.unsafe_mode=true`

Man behöver inte starta om Zoom. Skärmen borde nu synas som möjlig att dela. Lika så när man aktiverar `safe_mode` så fryses bilden för de som ser den utdelade skärmen. Inställningen återgår till att aktivera `safe_mode` vid omstart.

## Kan inte längre logga in via Wayland
https://ubuntuforums.org/showthread.php?t=2501544

