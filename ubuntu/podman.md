# podman

Referens: https://brandonrozek.com/blog/rootless-docker-compose-podman/

```bash
sudo apt install podman podman-docker docker-compose
```

We can then emulate the docker socket rootless with the following commands:

Emulera docker socket:

```bash
systemctl --user enable podman.socket
```

```bash
systemctl --user start podman.socket
```

Testa att emulering fungerar:

```bash
curl -H "Content-Type: application/json" \
	--unix-socket /var/run/user/$UID/podman/podman.sock \
    http://localhost/_ping
```

Du ska få tillbaks ett `OK`.

 We then need to create an environmental variable to tell docker compose where the emulated docker socket lives.

Konfigurera docker compose att använda podman:

```bash
export DOCKER_HOST=unix:///run/user/$UID/podman/podman.sock
```
